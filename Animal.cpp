#include "iostream"
#include "Animal.h"

void Animal::Voice()
{
	cout << "voice";
}

void Dog::Voice()
{
	cout << "Woof!";
}

void Cat::Voice()
{
	cout << "Meaw!";
}

void Mouse::Voice()
{
	cout << "Eek!";
}