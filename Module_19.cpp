#include <iostream>
#include "Animal.h"

using namespace std;
int main()
{   
    int size = 3;
    Animal **arr = new Animal*[size];

    arr[0] = new Dog;
    arr[1] = new Cat;
    arr[2] = new Mouse;

    for (int i = 0; i < size; i++)
    {
        arr[i]->Voice();
        cout << "\n";
    }
}
