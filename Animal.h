#include <iostream>
using namespace std;

class Animal
{

public:
	virtual void Voice();
};

class Dog : public Animal
{
public:
	void Voice() override;
};

class Cat : public Animal
{
public:
	void Voice() override;
};

class Mouse : public Animal
{
public:
	void Voice() override;
};
